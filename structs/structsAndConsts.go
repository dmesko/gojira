package structs

import (
	"github.com/fatih/color"
)

var JiraUrl = "https://jira.intgdc.com/rest/api/latest/"
var RestIssueCmd = "issue/"
var AgileUrl = "https://jira.intgdc.com/rest/greenhopper/1.0/"

var Team9BoardId = "256"

var Green = color.New(color.FgGreen).SprintFunc()
var Yellow = color.New(color.FgYellow).SprintFunc()
var Red = color.New(color.FgRed).SprintFunc()
var BgRed = color.New(color.BgRed).SprintFunc()
var BgYellow = color.New(color.BgYellow).SprintFunc()
var BgPink = color.New(color.BgHiMagenta).SprintFunc()

type Issue struct {
	Key    string
	Fields struct {
		Summary              string
		Timeoriginalestimate string
		Status               struct {
			Name string
		}
		Assignee struct {
			Name string
		}
	}
}

type Epics struct {
	Values []struct {
		Id   int64
		Key  string
		Name string
		Done bool
	}
}

type Sprints struct {
	Sprints []struct {
		Id    int64
		State string
		Name  string
	}
}

type SprintInfo struct {
	Contents struct {
		IssueKeysAddedDuringSprint        map[string]bool
		IssueKeysMissingRiskLabel         map[string]bool
		IssuesCompletedInAnotherSprint    []IssueInSprintInfo
		IssuesNotCompletedInCurrentSprint []IssueInSprintInfo
		CompletedIssues                   []IssueInSprintInfo
	}
	Sprint struct {
		EndDate string //"endDate": "06/Aug/15 2:49 PM", => 02/Jan/06 3:04pm
	}
}

type IssueInSprintInfo struct {
	Key      string
	Summary  string
	TypeName string
	Assignee string
	Status   struct {
		Name string
	}
	Late   bool
	NoRisk bool
	Custom struct {
		TextForm string
		FgAtt    string
		BgAtt    string
	}
}

type CorrectOrder []IssueInSprintInfo

func (a CorrectOrder) Len() int      { return len(a) }
func (a CorrectOrder) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a CorrectOrder) Less(i, j int) bool {
	// Typ Story ABOVE else
	// Late false ABOVE true
	// Status Verificatio ABOVE Hacking ABOVE other
	switch {
	case a[i].Status.Name == "Verification":
		return true
	case a[i].TypeName == a[j].TypeName:
		switch {
		case a[i].Late == a[j].Late:
			switch {
			case a[i].Status.Name == "Hacking" && a[j].Status.Name == "Verification":
				return false
			case a[i].Status.Name == "Hacking":
				return true
			default:
				return false
			}
		default:
			return !a[i].Late
		}
	case a[i].TypeName == "Story":
		return true
	default:
		return false
	}
}

type SubtaskInfo struct {
	Issues []struct {
		Key    string
		Fields struct {
			Summary string
			Created string
			Status  struct {
				Name string
			}
			Assignee struct {
				Name string
			}
		}
		Changelog struct {
			Total     int
			Histories []struct {
				Created string //"2015-08-06T13:25:52.817+0200"
				Items   []struct {
					Field      string
					FromString string
					ToString   string
				}
			}
		}
	}
}

type CorrectSubtasksOrder SubtaskInfo

func (a CorrectSubtasksOrder) Len() int      { return len(a.Issues) }
func (a CorrectSubtasksOrder) Swap(i, j int) { a.Issues[i], a.Issues[j] = a.Issues[j], a.Issues[i] }
func (a CorrectSubtasksOrder) Less(i, j int) bool {
	if a.Issues[i].Key > a.Issues[j].Key {
		return false
	}
	return true
}

type ForVerification struct {
	Total int
}

type LabelsInfoCount struct {
	Total  int
	Issues []struct {
		Key string
	}
}

type Config struct {
	Username  string
	Password  string
	BoardId   string
	Debug     bool
	SprintId  string
	TeamLabel string
	Refresh   struct {
		NormalRefresh  int
		NormalStart    string
		NormalEnd      string
		RelaxedRefresh int
	}
	Box      bool
	NoChecks bool
}

type TextToTransfer struct {
	Title             string
	CompletedLate     string
	CompletedCommited string
	Incompleted       string
	IncompletedFg     string
	IncompletedBg     string
	Unlabeled         int
	Unverified        int
	TeamLabel         string
	NoChecks          bool
}
