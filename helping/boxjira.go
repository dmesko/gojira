package helping

import (
	"bitbucket.org/dmesko/gojira/structs"
	"github.com/nsf/termbox-go"
)
import "time"
import "strings"
import "sync"
import "log"
import "fmt"

var data structs.TextToTransfer

func runeToAtt(ch rune) (att termbox.Attribute) {
	switch string(ch) {
	case "G":
		return termbox.ColorGreen
	case "Y":
		return termbox.ColorYellow
	case "R":
		return termbox.ColorRed
	case "K":
		return termbox.ColorBlack
	case "P":
		return termbox.ColorMagenta
	default:
		return termbox.ColorDefault
	}
}

func draw() {
	w, h := termbox.Size()
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	lineOffset := 1
	charOffset := w/2 + 1

	for charIndex, char := range data.Title {
		termbox.SetCell(charIndex, 0, char, termbox.ColorBlue, termbox.ColorDefault)
	}

	if data.Unlabeled != 0 && !data.NoChecks {
		var warning string
		if data.Unlabeled > 1 {
			warning = fmt.Sprintf("There are %d issues missing risk label", data.Unlabeled)
		} else {
			warning = "There is one issue missing risk label"
		}
		for charIndex, char := range warning {
			termbox.SetCell(charOffset+charIndex, 0, char, termbox.ColorBlue, termbox.ColorYellow|termbox.AttrBold)
		}
	}

	if !data.NoChecks {
		verificationNotice := ""
		if data.Unverified == 0 {
			verificationNotice = "There are not unverified issues - WOW"
			for charIndex, char := range verificationNotice {
				termbox.SetCell(w/2+charIndex, h-1, char, termbox.ColorGreen, termbox.ColorDefault)
			}
		} else {
			url := "t9-verify"
			if data.TeamLabel == "scrum8" {
				url = "t8-verify"
			}
			verificationNotice = fmt.Sprintf("There are %d unverified issues, for list check http://gg.gg/%s", data.Unverified, url)
			for charIndex, char := range verificationNotice {
				termbox.SetCell(w/2+charIndex, h-1, char, termbox.ColorYellow, termbox.ColorDefault)
			}
		}

	}

	if len(data.Incompleted) != len(data.IncompletedFg) || len(data.Incompleted) != len(data.IncompletedBg) {
		log.Fatalf("Incompleted data are not of the same length %d:%d:%d\n", len(data.Incompleted), len(data.IncompletedFg), len(data.IncompletedBg))
	}

	firstColumn := processLongMultilineString(data.Incompleted)
	fg := processLongMultilineString(data.IncompletedFg)
	bg := processLongMultilineString(data.IncompletedBg)
	for lineIndex, line := range firstColumn {
		for charIndex, char := range line {
			if charIndex > w/2 {
				break
			}

			termbox.SetCell(charIndex, lineIndex+lineOffset, char, runeToAtt([]rune(fg[lineIndex])[charIndex]), runeToAtt([]rune(bg[lineIndex])[charIndex]))
		}
	}

	secondColumn := processLongMultilineString("Completed issues\n" + data.CompletedCommited + data.CompletedLate)
	for lineIndex, line := range secondColumn {
		for charIndex, char := range line {
			if charIndex >= w/2 {
				break
			}
			termbox.SetCell(charIndex+charOffset, lineIndex+lineOffset, char, termbox.ColorDefault, termbox.ColorDefault)
		}
	}
	termbox.Flush()
}

func processLongMultilineString(input string) []string {
	replacer := strings.NewReplacer("\t", "    ")
	notabs := replacer.Replace(input)
	separateLines := strings.Split(notabs, "\n")
	return separateLines
}

// StartTermBox starts term box and initialize channels
func StartTermBox(textTransferChan chan structs.TextToTransfer, boxWaitingGroup *sync.WaitGroup, abortChan chan bool) {
	defer close(abortChan)
	defer boxWaitingGroup.Done()
	err := termbox.Init()
	if err != nil {
		panic(err)
	}
	log.Println("Termbox lib initialized")
	defer termbox.Close()
	eventQueue := make(chan termbox.Event)
	go func() {
		for {
			eventQueue <- termbox.PollEvent()
		}
	}()

	log.Print("Receiving data to print..")
	data = <-textTransferChan
	log.Println("have it")
	draw()
loop:
	for {
		select {
		case ev := <-eventQueue:
			if ev.Type == termbox.EventKey && ev.Key == termbox.KeyEsc {
				break loop
			}
		case <-abortChan: // signal from the main thread
			break loop
		default:
			select {
			case data = <-textTransferChan:
			case <-time.After(1 * time.Second):
			}
			draw()
		}
	}
	log.Println("Box goroutine terminating")

}
