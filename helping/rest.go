package helping

import (
	"encoding/json"
	"log"
	"math"
	"net/http"
	"time"
)

var client = &http.Client{}

type Credentials struct {
	Username string
	Password string
}

var Config Credentials

func InitRest(config Credentials) {
	Config = config
}

func CallRestAPI(urlQuery string, mappingStruct interface{}) error {
	username := Config.Username
	password := Config.Password

	req, err := http.NewRequest("GET", urlQuery, nil)
	req.SetBasicAuth(username, password)

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error!\n%s", err.Error())
		return err
	}
	defer resp.Body.Close()

	log.Println("JIRA response for query:", resp)
	err = json.NewDecoder(resp.Body).Decode(mappingStruct)
	if err != nil {
		log.Printf("Decoding Error!\n%s", err.Error())
		return err
	}
	return nil
}

// CallRestAPIWithBackoff calls api and retries for 4 times
func CallRestAPIWithBackoff(urlQuery string, mappingStruct interface{}) (err error) {
	for retry := 1; retry <= 4; retry++ {
		if delay := Backoff(retry); delay > 0 {
			log.Printf("After %ds delay trying for %d. time\n", delay, retry)
			time.Sleep(time.Duration(delay) * time.Second)
		}

		err = CallRestAPI(urlQuery, mappingStruct)
		if err == nil {
			return
		}
	}
	log.Println("Even after several retries request failed")
	return err
}

func Backoff(retryInt int) int {
	if retryInt == 1 {
		return 0
	}
	retry := float64(retryInt)
	return int((math.Exp2(retry) - 1) / 2)
}
