package jirafn

import (
	"fmt"
	"log"
	"net/url"

	"bitbucket.org/dmesko/gojira/helping"
	"bitbucket.org/dmesko/gojira/structs"
)

// GetIssuesWithoutRiskLabel returns issues missing risk label
func GetIssuesWithoutRiskLabel(teamLabel string) (*map[string]bool, error) {
	//https://jira.intgdc.com/rest/api/latest/search?jql=issuetype in (Bug, Chore, Story) AND labels not in ("q/regrisk/low", "q/risk/low", "q/regrisk/medium", "q/risk/medium", "q/regrisk/high", "q/risk/high", "q/regrisk/none", "q/risk/none") AND labels = scrum9 AND sprint in openSprints() AND status in (Hacking, Verification, Completed)
	searchQuery := fmt.Sprintf("issuetype in (Bug, Chore, Story) AND labels not in (\"q/regrisk/low\", \"q/risk/low\", \"q/regrisk/medium\", \"q/risk/medium\", \"q/regrisk/high\", \"q/risk/high\", \"q/regrisk/none\", \"q/risk/none\") AND labels = \"%s\" AND sprint in openSprints() AND status in (Hacking, Verification, Completed)", teamLabel)
	log.Println("Getting number of issues missing risk label")
	urlQuery := structs.JiraUrl + fmt.Sprintf("search?jql=%s", url.QueryEscape(searchQuery))
	log.Println("REST url:", urlQuery)
	labelsInfo := new(structs.LabelsInfoCount)
	set := make(map[string]bool)
	if err := helping.CallRestAPIWithBackoff(urlQuery, &labelsInfo); err != nil {
		return &set, err
	}
	for _, s := range labelsInfo.Issues {
		set[s.Key] = true
	}

	log.Println("Info returned:", set)
	return &set, nil
}

// GetSubtasks returns subtasks for story/issue
func GetSubtasks(issueID string) (subtaskInfo structs.SubtaskInfo, err error) {
	//	https://jira.intgdc.com/rest/api/latest/search?jql=parent=PAAS-424
	log.Println("Getting subtasks for the issue:", issueID)
	urlQuery := structs.JiraUrl + fmt.Sprintf("search?jql=parent=%s&expand=changelog", issueID)
	log.Println("REST url:", urlQuery)
	if err = helping.CallRestAPIWithBackoff(urlQuery, &subtaskInfo); err != nil {
		return subtaskInfo, err
	}
	log.Println("Info returned:", subtaskInfo)
	return subtaskInfo, nil
}

// GetSprintInfo returns info about sprint: Team, sprint name, time
func GetSprintInfo(boardID string, sprintID string, noRetry bool) (sprintInfo structs.SprintInfo, err error) {
	//https://jira.intgdc.com/rest/greenhopper/latest/rapid/charts/sprintreport?rapidViewId=256&sprintId=377
	log.Println("Getting active sprint info")
	urlQuery := structs.AgileUrl + fmt.Sprintf("rapid/charts/sprintreport?rapidViewId=%s&sprintId=%s", boardID, sprintID)
	log.Println("REST url:", urlQuery)
	if noRetry {
		err = helping.CallRestAPI(urlQuery, &sprintInfo)
	} else {
		err = helping.CallRestAPIWithBackoff(urlQuery, &sprintInfo)
	}
	log.Println("Info returned:", sprintInfo)
	return sprintInfo, err
}

// GetNumberOfTicketsForVerification ask jira for number of issues labeled with teamLabel in project PAAS and state Verification
func GetNumberOfTicketsForVerification(teamLabel string) (ticketCount int, err error) {
	////https://jira.intgdc.com/rest/api/latest/search?jql=project = \"PAAS\" AND labels = \"scrum9\" AND status = \"Verification\"
	log.Println("Searching number of tickets for verification for label", teamLabel)
	urlQuery := structs.JiraUrl + "search?jql=" + url.QueryEscape(fmt.Sprintf("project = \"PAAS\" AND labels = \"%s\" AND status = \"Verification\"", teamLabel))
	log.Println("REST url:", urlQuery)

	var forVerification structs.ForVerification
	if err = helping.CallRestAPI(urlQuery, &forVerification); err != nil {
		return -1, err
	}
	log.Println("For verification returned:", forVerification.Total)
	return forVerification.Total, nil
}

// GetActiveSprint returns running/active sprint for given scrum board
func GetActiveSprint(boardID string) (activeSprintID string, sprintName string, err error) {
	//https://jira.intgdc.com/rest/greenhopper/1.0/sprintquery/256
	log.Println("Searching active sprint for Board Id:", boardID)
	urlQuery := structs.AgileUrl + "sprintquery/" + boardID
	log.Println("REST url:", urlQuery)

	var sprints structs.Sprints
	if err = helping.CallRestAPI(urlQuery, &sprints); err != nil {
		return "", "", err
	}
	log.Println("Sprints returned:", sprints)

	for _, sprint := range sprints.Sprints {
		if sprint.State == "ACTIVE" {
			log.Printf("%s %s %d", sprint.State, sprint.Name, sprint.Id)
			return fmt.Sprintf("%v", sprint.Id), sprint.Name, nil
		}
	}
	log.Fatalln("Active sprint not found")
	return
}
