package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"

	"bitbucket.org/dmesko/gojira/helping"
	"bitbucket.org/dmesko/gojira/jirafn"
	"bitbucket.org/dmesko/gojira/structs"
	"github.com/fatih/color"
	"golang.org/x/crypto/ssh/terminal"
)

var subtaskProcessingGroup sync.WaitGroup
var boxWaitingGroup sync.WaitGroup

// Config is gojira configuration structre
var Config structs.Config

func main() {
	processParameters()
	helping.InitRest(helping.Credentials{Username: Config.Username, Password: Config.Password})
	if !Config.Debug {
		log.SetOutput(ioutil.Discard)
	}
	var sprintName string
	var err error
	if Config.SprintId == "" {
		Config.SprintId, sprintName, err = jirafn.GetActiveSprint(Config.BoardId)
		if err != nil {
			fmt.Println("Can't get active sprint, error:", err)
			return
		}
	} else {
		sprintName = "sprint #" + Config.SprintId
	}
	log.Println("Active sprint ID:", Config.SprintId)

	sprintTitle, sprintInfo, err := getSprintTitleAndInfo(sprintName, Config.BoardId, Config.SprintId, true)
	if err != nil {
		fmt.Println("Can't get sprint title and info, error:", err)
		return
	}

	if Config.Box {
		color.NoColor = true
		textTransferChan := make(chan structs.TextToTransfer, 1)
		abortChan := make(chan bool, 1)
		log.Println("Starting termbox goroutine")
		boxWaitingGroup.Add(1)
		go helping.StartTermBox(textTransferChan, &boxWaitingGroup, abortChan)
	thisForLoop:
		for {
			sprintTitle, sprintInfo, err = getSprintTitleAndInfo(sprintName, Config.BoardId, Config.SprintId, false)

			unlabeledMap, errLabel := (new(map[string]bool)), error(nil)
			unverified := 0

			if !Config.NoChecks {
				unlabeledMap, errLabel = jirafn.GetIssuesWithoutRiskLabel(Config.TeamLabel)
				unverified, _ = jirafn.GetNumberOfTicketsForVerification(Config.TeamLabel)
				sprintInfo.Contents.IssueKeysMissingRiskLabel = *unlabeledMap
			}

			if err != nil || errLabel != nil {
				log.Println("Can't get sprint title and info or count of unlabeled issues, error:", err)
				log.Println("Sending abort signal to the termbox goroutine")
				abortChan <- true
				break thisForLoop
			} else {
				incompleted, ifg, ibg := mergeIncompletedToString(sprintInfo)
				commited, late := formatCompletedIssue(sprintInfo)
				data := structs.TextToTransfer{Title: sprintTitle,
					CompletedLate:     late,
					CompletedCommited: commited,
					Incompleted:       incompleted,
					IncompletedFg:     ifg,
					IncompletedBg:     ibg,
					Unlabeled:         len(*unlabeledMap),
					Unverified:        unverified,
					TeamLabel:         Config.TeamLabel,
					NoChecks:          Config.NoChecks}
				log.Printf("Sending data to the termbox goroutine: %+v \n", data)
				textTransferChan <- data

				select {
				case <-abortChan:
					break thisForLoop
				case <-time.After(time.Duration(refreshRate()) * time.Second):
				}
			}

		}

		boxWaitingGroup.Wait()
		log.Println("Box goroutine waiting finished")
	} else {
		fmt.Println(sprintTitle)
		printIncompletedIssues(sprintInfo)
		printCompletedIssues(sprintInfo)
	}
	if err != nil {
		fmt.Println("Exiting with error:", err)
	}
	fmt.Println("end.")
}

func refreshRate() int {
	//	h,m,_ := time.Now().Clock()
	//	NOW := 60*h+m
	//	S, _ := time.Parse("15:04", Config.refresh.normalStart)
	//	SH, SM, _ := S.Clock()
	//	STIME = 60*h+m
	//	E, _ := time.Parse("15:04", Config.refresh.normalEnd)
	//	EH, EM, _ := E.Clock()
	//	ETIME = 60*h+m
	//
	//	if STIME < NOW && NOW < ETIME {
	//
	//	}

	// now := time.Now()
	// today := now.Truncate(time.Duration(24) * time.Hour)
	// todaySeconds := now.Unix() - today.Unix()
	// startSeconds, _ := time.Parse("15:04", Config.Refresh.NormalStart)
	// endSeconds, _ := time.Parse("15:04", Config.Refresh.NormalEnd)
	// refresh := Config.Refresh.RelaxedRefresh
	// if startSeconds.Unix() < todaySeconds && todaySeconds < endSeconds.Unix() {
	// 	refresh = Config.Refresh.NormalRefresh
	// }
	return Config.Refresh.NormalRefresh
}

func getSprintTitleAndInfo(sprintName, boardID, sprintID string, noRetry bool) (sprintTitle string, sprintInfo structs.SprintInfo, err error) {
	sprintTitle = fmt.Sprint("State of ", sprintName, " at ", time.Now().Format("15:04 2.1.2006")) //01/02 03:04:05PM '06 -0700
	sprintInfo, err = jirafn.GetSprintInfo(boardID, sprintID, noRetry)
	return
}

func formatCompletedIssue(sprintInfo structs.SprintInfo) (committed, late string) {
	allCompleted := append(sprintInfo.Contents.CompletedIssues, sprintInfo.Contents.IssuesCompletedInAnotherSprint...)
	for _, issue := range allCompleted {
		_, addedLater := sprintInfo.Contents.IssueKeysAddedDuringSprint[issue.Key]
		_, noRiskLabel := sprintInfo.Contents.IssueKeysMissingRiskLabel[issue.Key]

		verificationFlag := " "
		if issue.Status.Name == "Verification" {
			verificationFlag = "V"
		}

		if addedLater {
			late += fmt.Sprintln(chooseTheFlag(addedLater, noRiskLabel), verificationFlag, issue.Key, issue.Summary)
		} else {
			committed += fmt.Sprintln(chooseTheFlag(addedLater, noRiskLabel), verificationFlag, issue.Key, issue.Summary)
		}
	}
	return committed, late
}

func chooseTheFlag(addedLater, noRiskLabel bool) string {
	if noRiskLabel {
		return "R"
	}
	if addedLater {
		return "*"
	}
	return " "
}

func printCompletedIssues(sprintInfo structs.SprintInfo) {
	fmt.Println(structs.Green("\nCompleted issues"))
	fmt.Print(formatCompletedIssue(sprintInfo))
}

func formatIssueAndSubtasks(issue *structs.IssueInSprintInfo) (err error) {
	flag := " "
	if issue.Late {
		flag = "*"
	}

	if issue.NoRisk {
		flag = "R"
	}

	bgc := " "
	switch issue.Status.Name {
	case "Backlog":
		flag = structs.BgRed(flag)
		bgc = "R"
	case "Hacking":
		flag = structs.BgYellow(flag)
		bgc = "Y"
	case "Verification":
		flag = "V"
		flag = structs.BgPink(flag)
		bgc = "P"
	}
	result := fmt.Sprintln(flag, issue.Key, issue.Summary)
	fg := fmt.Sprintln(toAt(flag, "K"), toAt(issue.Key, " "), toAt(issue.Summary, " "))
	bg := fmt.Sprintln(toAt(flag, bgc), toAt(issue.Key, " "), toAt(issue.Summary, " "))

	var Tresult, Tfg, Tbg string
	if Tresult, Tfg, Tbg, err = formatSubtasks(issue.Key); err != nil {
		return err
	}

	issue.Custom.TextForm = result + Tresult
	issue.Custom.FgAtt = fg + Tfg
	issue.Custom.BgAtt = bg + Tbg
	return nil
}

func formatSubtasks(issueKey string) (result, fg, bg string, err error) {
	var subtasks structs.SubtaskInfo
	if subtasks, err = jirafn.GetSubtasks(issueKey); err != nil {
		return "", "", "", err
	}
	sort.Sort(structs.CorrectSubtasksOrder(subtasks))
	for _, stask := range subtasks.Issues {
		status := structs.Green(stask.Fields.Status.Name)
		fgc := "G"
		switch stask.Fields.Status.Name {
		case "In Progress":
			status = structs.Yellow(stask.Fields.Status.Name)
			fgc = "Y"
		case "ToDo":
			stask.Fields.Status.Name = "To Do"
			status = structs.Red(stask.Fields.Status.Name)
			fgc = "R"
		}
		result += fmt.Sprintln("    \\_", stask.Key, status, stask.Fields.Summary)
		fg += fmt.Sprintln(toAt("    \\_", " "), toAt(stask.Key, " "), toAt(stask.Fields.Status.Name, fgc), toAt(stask.Fields.Summary, " "))
		bg += fmt.Sprintln(toAt("    \\_", " "), toAt(stask.Key, " "), toAt(stask.Fields.Status.Name, " "), toAt(stask.Fields.Summary, " "))
	}
	return
}

func mergeIncompletedToString(sprintInfo structs.SprintInfo) (result, fg, bg string) {
	inc := "Issues to do"
	result = fmt.Sprintln(inc)
	fg = fmt.Sprintln(toAt(inc, "Y"))
	bg = fmt.Sprintln(toAt(inc, " "))
	issues := getIncompletedIssuesSubtasks(sprintInfo)
	for _, issue := range issues {
		result += fmt.Sprint(issue.Custom.TextForm)
		fg += issue.Custom.FgAtt
		bg += issue.Custom.BgAtt
	}
	return result, fg, bg
}

func printIncompletedIssues(sprintInfo structs.SprintInfo) {
	fmt.Println(structs.Yellow("Issues to do"))
	issues := getIncompletedIssuesSubtasks(sprintInfo)
	for _, issue := range issues {
		fmt.Print(issue.Custom.TextForm)
	}
}

func getIncompletedIssuesSubtasks(sprintInfo structs.SprintInfo) []structs.IssueInSprintInfo {

	issueChan := make(chan *structs.IssueInSprintInfo, runtime.NumCPU())
	for idx := range sprintInfo.Contents.IssuesNotCompletedInCurrentSprint {
		issue := &sprintInfo.Contents.IssuesNotCompletedInCurrentSprint[idx]
		_, issue.Late = sprintInfo.Contents.IssueKeysAddedDuringSprint[issue.Key]
		_, issue.NoRisk = sprintInfo.Contents.IssueKeysMissingRiskLabel[issue.Key]
		log.Printf("Sending issue %s to the channel for processing (%p) \n", issue.Key, issue)
		subtaskProcessingGroup.Add(1)
		go receiveIssue(issueChan)
		issueChan <- issue
	}
	subtaskProcessingGroup.Wait()
	sort.Sort(structs.CorrectOrder(sprintInfo.Contents.IssuesNotCompletedInCurrentSprint))

	return sprintInfo.Contents.IssuesNotCompletedInCurrentSprint
}

func receiveIssue(issueChan chan *structs.IssueInSprintInfo) {
	log.Print("Receiving issue from the channel...")
	issue := <-issueChan
	log.Printf("%s received %p \n", issue.Key, issue)
	if err := formatIssueAndSubtasks(issue); err != nil {
		log.Printf("Formating of issue %s failed with error %s\n", issue.Key, err)
	}
	subtaskProcessingGroup.Done()
}

func sprintDecodeForTest(data string, sp structs.Sprints) structs.Sprints {
	err := json.Unmarshal([]byte(data), &sp)
	if err != nil {
		log.Fatalf("Decoding Error!\n%s", err.Error())
	}
	return sp
}

func processParameters() {
	username := flag.String("user", "dezider.mesko", "JIRA username")
	boardID := flag.String("board", "256", "JIRA board id (you can find it in URL as rapidView)")
	sprintID := flag.String("sprint", "", "Force sprint to use (you can find it in Sprint report URL)")
	debug := flag.Bool("debug", false, "show log messages to stderr")
	box := flag.Bool("box", false, "use TermBox UI")
	refresh := flag.Int("refresh", 30, "seconds to refresh, minimum is 10")
	teamLabel := flag.String("teamlabel", "scrum9", "Label to count unverified and missing risk label issues")
	noChecks := flag.Bool("noChecks", false, "Skip verification and risk labels checking. This checking needs team label")
	flag.Parse()
	Config.NoChecks = *noChecks
	Config.TeamLabel = *teamLabel
	Config.BoardId = *boardID
	Config.Username = *username
	Config.Debug = *debug

	Config.Password = os.Getenv("pass")
	Config.SprintId = *sprintID
	Config.Refresh.NormalRefresh = *refresh
	if Config.Refresh.NormalRefresh < 10 {
		Config.Refresh.NormalRefresh = 10
	}
	Config.Refresh.RelaxedRefresh = 60 * 60
	Config.Refresh.NormalStart = "07:30"
	Config.Refresh.NormalEnd = "18:30"
	Config.Box = *box
	if Config.Password == "" {
		fmt.Printf("%s, tell me your secret: ", Config.Username)
		pass, _ := terminal.ReadPassword(0)
		Config.Password = string(pass)
		fmt.Println()
	}
}

func toAt(text string, attrib string) string {
	return strings.Repeat(attrib, len(text))
}
