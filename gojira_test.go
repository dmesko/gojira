package main

import (
	"fmt"
	"testing"

	"bitbucket.org/dmesko/gojira"
	"bitbucket.org/dmesko/gojira/structs"
)

func TestSprintDecode(t *testing.T) {
	data := `{"sprints":[{"id":367,"sequence":367,"name":"T9 23/7 - 30/7 S15","state":"CLOSED","linkedPagesCount":0},{"id":377,"sequence":377,"name":"T9 31/7 - 6/8 S16","state":"ACTIVE","linkedPagesCount":0}],"rapidViewId":256}`

	var sprt structs.Sprints
	sprt = sprintDecodeForTest(data, sprt)
	fmt.Println(sprt)
	fmt.Println(sprt.Sprints[0].State, " ", sprt.Sprints[1].State)
}

func TestBackoff(t *testing.T) {
	retries := 5
	for i := 0; i < retries; i++ {
		fmt.Println("For", i, "sleep", gojira.Backoff(i), "seconds")
	}
}

func TestRelaxedTime(t *testing.T) {
	//	config = &Config{refresh:{30, "8:00", "9:00", 600}}
	//	if refreshRate() != 30 {
	//		t.Fail()
	//	}
}
